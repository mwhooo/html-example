param(
    [Parameter(Mandatory=$false)] [string] $sendto = "mwhooo@gmail.com",
    [Parameter(Mandatory=$false)] [string] $sendfrom = "mwhooo@gmail.com",
    [Parameter(Mandatory=$false)] [string] $smptserver = "smtp.telfort.nl"
)

$CWD = Split-Path $MyInvocation.MyCommand.Definition

#OPTION1
#example of 3 headers using CSS and precontent and postcontent 1 example
$x = Get-Process | Select -First 10 | ConvertTo-HTML -Title "Processes" -PreContent "<h1>Process overview</h1><hr>" `
                   -PostContent "<h3>End Process overview</h3>"
$y = Get-Service | Select -First 10 | ConvertTo-HTML -Title "Services" -PreContent "<h1>Service</h1>"
$z = Get-WmiObject -class Win32_OperatingSystem | ConvertTo-HTML -Property * -Title "Report OS" -PreContent "<h1>Report OS</h1><hr>"
$file1 = "$CWD\HtmlReport1.html"
ConvertTo-HTML -body "$x $y $z" -CSSUri "$CWD\test.css" | Set-Content $file1
explorer $file1


#OPTION2
#just dump all services in nice HTML overview based on a CSS for demo purposes anad using simple example
$file2 = "$CWD\HtmlReport2.html"
Get-Service | ConvertTo-Html -CSSUri "$CWD\test.css" -Title "Overview for : $($ENV:COMPUTERNAME)" -PreContent "Report for : $($ENV:COMPUTERNAME) on $(get-date)" | Set-Content $file2
#explorer $file2

#OPTION3 Based on the HTML herestring on top and fragments

$Header = @"
<style>
table {
font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
border-collapse: collapse;
width: 100%;
}
th {
padding-top: 12px;
padding-bottom: 12px;
text-align: left;
background-color: #4CAF50;
color: Grey;
}
</style>
"@


$file3 = "$CWD\HtmlReport3.html"
$OS = Get-WmiObject -class Win32_OperatingSystem | select Caption,version,OSArchitecture,NumberOfUsers,LastBootUpTime | ConvertTo-HTML -Fragment
$Bios = Get-WmiObject -class Win32_BIOS | select Version, SoftwareElementID, ReleaseDate, Manufacturer, BIOSVersion | ConvertTo-HTML -Fragment
$Services = Get-WmiObject -class Win32_Service | select name, displayname, state | ConvertTo-HTML -Fragment
ConvertTo-HTML -Body "$OS $Bios $Services" -Title "$($ENV:COMPUTERNAME) on $(get-date) " -Head $Header | Out-File $file3
#explorer $file3

#./StatusReport.html


$body = @"
Dear Mark,

the following report has been automatically send by you :-P

and it ran on $ENV:COMPUTERNAME
kind regards

Mark 
"@

$mailsubject = "Report from server $($ENV:COMPUTERNAME) on $(get-date)"
Send-MailMessage -To $sendto -From $sendfrom -Attachments $file1, $file2, $file3  -Subject $mailsubject -SmtpServer $smptserver -Body $body



break















break










Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Windows.Forms.DataVisualization

Function Invoke-SaveDialog {
    $FileTypes = [enum]::GetNames('System.Windows.Forms.DataVisualization.Charting.ChartImageFormat')| ForEach {
        $_.Insert(0,'*.')
    }
    $SaveFileDlg = New-Object System.Windows.Forms.SaveFileDialog
    $SaveFileDlg.DefaultExt='PNG'
    $SaveFileDlg.Filter=";Image Files ($($FileTypes))|$($FileTypes)|All Files (*.*)|*.*";
    $return = $SaveFileDlg.ShowDialog()
    If ($Return -eq 'OK') {
        [pscustomobject]@{
            FileName = $SaveFileDlg.FileName
            Extension = $SaveFileDlg.FileName -replace '.*\.(.*)','$1'
        }
 
    }
}


$t = Get-Eventlog Application | Group-Object -property InstanceId | select -First 10

             #@{Label=”Last access”;Expression={($_.lastwritetime).ToshortDateString()}}
             #Get-Eventlog Application | Sort-Object InstanceId

$Processes = Get-Process |
Sort-Object WS -Descending |
Select-Object -First 10


$Chart = New-object System.Windows.Forms.DataVisualization.Charting.Chart
$ChartArea = New-Object System.Windows.Forms.DataVisualization.Charting.ChartArea
$Series = New-Object -TypeName System.Windows.Forms.DataVisualization.Charting.Series
$ChartTypes = [System.Windows.Forms.DataVisualization.Charting.SeriesChartType]

$Series.ChartType = $ChartTypes::Pie
$Chart.Series.Add($Series)
$Chart.ChartAreas.Add($ChartArea)
#$Chart.Series['Series1'].Points.DataBindXY($t.Name, $t.Count)

$Chart.Series['Series1'].Points.DataBindXY($Processes.Name, $Processes.WS)

$Chart.Width = 700
$Chart.Height = 400
$Chart.Left = 10
$Chart.Top = 10
$Chart.BackColor = [System.Drawing.Color]::White
$Chart.BorderColor = 'Black'
$Chart.BorderDashStyle = 'Solid'
$ChartTitle = New-Object System.Windows.Forms.DataVisualization.Charting.Title
$ChartTitle.Text = 'Top 10 Processes by Working Set Memory'
$Font = New-Object System.Drawing.Font @('Microsoft Sans Serif','12', [System.Drawing.FontStyle]::Bold)
$ChartTitle.Font =$Font
$Chart.Titles.Add($ChartTitle)
$Legend = New-Object System.Windows.Forms.DataVisualization.Charting.Legend
$Legend.IsEquallySpacedItems = $True
$Legend.BorderColor = 'Black'

$Chart.Legends.Add($Legend)

#$chart.Series["Series1"].LegendText = "#VALX (#VALY)"

$Chart.Series['Series1']['PieLineColor'] = 'Black'
$Chart.Series['Series1']['PieLabelStyle'] = 'Outside'
$Chart.Series['Series1'].Label = "#VALX (#VALY)"

#region Windows Form to Display Chart
$AnchorAll = [System.Windows.Forms.AnchorStyles]::Bottom -bor [System.Windows.Forms.AnchorStyles]::Right -bor
    [System.Windows.Forms.AnchorStyles]::Top -bor [System.Windows.Forms.AnchorStyles]::Left
$Form = New-Object Windows.Forms.Form
$Form.Width = 740
$Form.Height = 490
$Form.controls.add($Chart)
$Chart.Anchor = $AnchorAll
 
# add a save button
$SaveButton = New-Object Windows.Forms.Button
$SaveButton.Text = "Save"
$SaveButton.Top = 420
$SaveButton.Left = 600
$SaveButton.Anchor = [System.Windows.Forms.AnchorStyles]::Bottom -bor [System.Windows.Forms.AnchorStyles]::Right
# [enum]::GetNames('System.Windows.Forms.DataVisualization.Charting.ChartImageFormat')
$SaveButton.add_click({
    $Result = Invoke-SaveDialog
    If ($Result) {
        $Chart.SaveImage($Result.FileName, $Result.Extension)
    }
})
 
$Form.controls.add($SaveButton)
$Form.Add_Shown({$Form.Activate()})
[void]$Form.ShowDialog()
#endregion Windows Form to Display Chart

